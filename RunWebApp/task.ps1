param()
$connectionString = Get-VstsInput -Name connectionString -Require
$ErrorActionPreference = "Stop"

# Check if .NET Core SDK is installed
try {
    $dotnetVersion = dotnet --version
    Write-Host "Found .NET Core SDK version $dotnetVersion"
} catch {
    Write-Host "Could not find .NET Core SDK"
    exit 1
}

#Delete folder if exists
if (Test-Path ".\WebApp") {
    Remove-Item -Recurse -Force ".\WebApp"
}

# Unzip web app
Expand-Archive -Path ".\WebApp.zip" -DestinationPath ".\WebApp"

# Navigate to the project directory
Set-Location ".\WebApp"

# Run the web app
$env:ASPNETCORE_URLS = "https://localhost:5004"
$env:CONNECTION_STRING = $connectionString
dotnet DXApplication1.Blazor.Server.dll