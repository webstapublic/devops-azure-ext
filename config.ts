window.requirejs.config({
    enforceDefine: true,
    paths: {
        'SDK': './lib/SDK.min'
    }
});

window.requirejs(['SDK'], async function (SDK: any) {
    console.log("SDK loaded");
    if (typeof SDK !== 'undefined') {
        await SDK.init();
        await SDK.ready();
        const accessToken = await SDK.getAccessToken();

        const extDataService = await SDK.getService("ms.vss-features.extension-data-service");
        const dataManager = await extDataService.getExtensionDataManager(SDK.getExtensionContext().id, accessToken);

        const value = await dataManager.getValue("variable");
        if (value) {
            (document.getElementById("variable-input") as HTMLInputElement).value = value;
        }

        document.getElementById("save-button")!.addEventListener("click", async () => {
            const value = (document.getElementById("variable-input") as HTMLInputElement).value;
            await dataManager.setValue("variable", value);

            const messageService = await SDK.getService("ms.vss-web.dialog-service");
            messageService.openMessageDialog("Variable saved", { title: "Success" });
        });
    } else {
        console.log('SDK is not defined');
    }
});