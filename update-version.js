const fs = require('fs');
const path = require('path');

const vssExtensionJson = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'vss-extension.json'), 'utf8'));
const versionSplits = vssExtensionJson.version.split('.');
vssExtensionJson.version = `${versionSplits[0]}.${versionSplits[1]}.${parseInt(versionSplits[2]) + 1}`;
fs.writeFileSync(path.resolve(__dirname, 'vss-extension.json'), JSON.stringify(vssExtensionJson, null, 2));

const taskJson = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'RunWebApp/task.json'), 'utf8'));
taskJson.version.Patch = parseInt(taskJson.version.Patch) + 1;
fs.writeFileSync(path.resolve(__dirname, 'RunWebApp/task.json'), JSON.stringify(taskJson, null, 2));